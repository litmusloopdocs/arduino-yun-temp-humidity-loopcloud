
/*
 Litmus Loop
*/

/*
Inlcude all the library files 
*/
#include <loopmq.h>
#include <Bridge.h>
#include <BridgeClient.h>
#include <SPI.h>
#include <stdlib.h>
#include <stdio.h>
#include <ArduinoJson.h>
#include "DHT.h"
#include <Wire.h>
#include "configuration.h"

#define DHTPIN A0     // what pin we're connected to

// Uncomment whatever type you're using!
//#define DHTTYPE DHT11   // DHT 11 
#define DHTTYPE DHT22   // DHT 22  (AM2302)
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

DHT dht(DHTPIN, DHTTYPE);


/*
Passing the defined information to char
*/
int port = port_number;                       // port number
char hostname[] = server;                     // Server name
char c[15] = clientID;                        // ClientID
char pass[16]= password;                      // password
char user[16] = userID;                       // username
char p[]= subTOPIC;                           // Subscribe on this topic to get the data
char s[]= pubTopic;                           // Publish on this topic to send data or command to device 

/*
Function to display the message that are given by the user to the device. 
*/

void callback(char* topic, byte* payload, unsigned int length) {
 Serial.print("Message arrived [");
 Serial.print(topic);
 Serial.print("] ");

/*
Display the message published serially and on the LCD
*/
  for (int i=0;i<length;i++) {
    Serial.print((char)payload[i]);           // Serial Display
  }
     Serial.println();                         //Print on a new line
}


BridgeClient yunClient;                       // yun client
PubSubClient loopmq(yunClient);               // instance to use the loopmq functions.

void reconnect() {                        
   
  while (!loopmq.connected()) {               // Loop until we're reconnected
  Serial.print("Attempting MQTT connection...");    // Attempt to connect    
    if (loopmq.connect(c,user,pass)) {
    Serial.println("connected");            // Once connected, publish an announcement...
            
         loopmq.subscribe(s);                 // Subscribe to a topic  
/*  
To Unscubscribe from a Topic uncomment the code below   
*/
        
   // loopmq.unsubscribe(s); 
   
/*
To disconnect unocmment the code below
*/
      
   //  loopmq.disconnect();            
    } 
    else {
   Serial.print("failed, rc=");
   Serial.print(loopmq.state());
   Serial.println(" try again in 5 seconds");
      
      delay(5000);                            // Wait 5 seconds before retrying
    }
  }
}

void setup()
{

  /* 
   Bridge takes about two seconds to start up
   it can be helpful to use the on-board LED
   as an indicator for when it has initialized
   */
   
  pinMode(13, OUTPUT);
  
  digitalWrite(13, LOW);
  
  Bridge.begin();
  
  digitalWrite(13, HIGH);
  
  Serial.begin(9600); 

  loopmq.setServer(hostname, port);           // Connect to the specified server and port defined by the user
  loopmq.setCallback(callback);               // Call the callbeck funciton when published     

  dht.begin();                                // sensor 
    
  delay(1500);                                // Allow the hardware to sort itself out

}

void loop()
{

/*
Temperature and Humidity Sensor

Reading temperature or humidity takes about 250 milliseconds!
Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
*/

    float h = dht.readHumidity();
    float t = dht.readTemperature();

    if (isnan(t) || isnan(h))                 // check if returns are valid, if they are NaN (not a number) then something went wrong!
    {
        Serial.println("Failed to read from DHT");
    } 
    else 
    {
        Serial.print("Humidity: "); 
        Serial.print(h);
        Serial.print(" %\t");
        Serial.print("Temperature: "); 
        Serial.print(t);
        Serial.println(" *C");
    }


/*
JSON parser
*/
  StaticJsonBuffer<200> jsonBuffer;               //  Inside the brackets, 200 is the size of the pool in bytes.If the JSON object is more complex, you need to increase that value. 

  JsonObject& root = jsonBuffer.createObject();   // It's a reference to the JsonObject, the actual bytes are inside the JsonBuffer with all the other nodes of the object tree.
  root["command"] = "Temperature and Humidity";                     // Add values in the object, add the objects you want to add to the JSON in the form of root["key'] = value;
  root["data"]= "";

  JsonObject& data= root.createNestedObject("data"); // nested JSON 
  data["Humidity"]=h;                                // Add data["key"]= value
  data["Temperature"]=t;


  root.printTo(Serial);                           // prints to serial terminal
  Serial.println();

  char buffer[100];                               // buffer to pass as payload
  root.printTo(buffer, sizeof(buffer));           // copy the JSON to the buffer to pass as a payload 

/*
Publish to the server
*/
  
  if (!loopmq.connected()) {
    reconnect();                              // Try to reconnect if connection dropped
  }
 if (loopmq.connect(c, user, pass)){

   loopmq.publish(p,buffer);                     // Publish message to the server once only
     delay(1000);
  }
  
 loopmq.loop();                              // check if the network is connected and also if the client is up and running
}






